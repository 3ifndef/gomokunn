import subprocess
import threading
import time
from parse import parse

class BrainError(Exception):
    pass

class Brain:
    def __init__(self, path):
        self.path = path
        try:
            self.process = subprocess.Popen(self.path, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        except OSError:
            raise BrainError('Process didn\'t start')
        self.messages = []
        self.terminated = False
        self.reader_thread = threading.Thread(target=self.read_messages, daemon=True)
        self.reader_thread.start()

    def read_messages(self):
        while not self.terminated:
            for msg in self.process.stdout:
                self.messages.extend(msg.decode().split('\n'))

    def send(self, message):
        print('<<<', message)
        self.process.stdin.write((message +'\r\n').encode())
        self.process.stdin.flush()

    def start(self, board_size):
        self.send(f'START {board_size}')
        self.expect_response('OK')

    def end(self):
        self.send('END')

    def info(self, key, value):
        self.send(f'INFO {key} {value}')

    def begin(self):
        self.send('BEGIN')
    
    def turn(self, x, y):
        self.send(f'TURN {x},{y}')

    def terminate(self):
        state = self.process.poll()
        if state is None:
            self.end()
            self.process.wait()
        self.terminated = True

    def restart(self):
        self.send(f'RESTART')
        self.expect_response('OK')

    def read_line(self, timeout=1):
        start_time = time.time()
        while True:
            while len(self.messages) == 0:
                if time.time() - start_time > timeout:
                    return None
                time.sleep(0.01)
            line = self.messages.pop(0).strip()
            if line != '':
                return line

    def messages_iter(self):
        while True:
            if len(self.messages) == 0:
                return
            line = self.messages.pop(0).strip()
            if line != '':
                yield line

    def expect_response(self, expected, ignore_msgs=True):
        while True:
            response = self.read_line()
            if not response:
                continue
            if ignore_msgs and (response.startswith('MESSAGE') or response.startswith('DEBUG')):
                continue
            if response != expected:
                raise BrainError(f'Wrong response: \'{response}\', expected: \'{expected}\'')
            break
