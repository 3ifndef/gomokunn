import numpy as np

class Game:
    def __init__(self):
        self.state = [None for _ in range(225)]
        self.winner = None
        self.illegal_actions = [1 for i in range(225)]
        self.illegal_actions[7*15+7] = 0

    def reset(self):
        self.state = [None for _ in range(225)]
        self.winner = None
        self.illegal_actions = [1 for i in range(225)]
        self.illegal_actions[7*15+7] = 0

    def get_nn_input(self, player):
        return np.asarray([x == None for x in self.state] + [x == player for x in self.state] + [x == 1 - player for x in self.state], dtype=np.float32)

    def step(self, player, action):
        self.state[action] = player
        self.illegal_actions[action] = 2
        x, y = action % 15, action // 15
        for a in range(max(0, x - 2), min(self.size, x + 3)):
            for b in range(max(0, y - 2), min(self.size, y + 3)):
                if self.illegal_actions[b * 15 + a] == 1:
                    self.illegal_actions[b * 15 + a] = 0
        self.winner = self.check_win()

    def is_legal_action(self, action):
        return self.state[action] == None

    def is_in_bounds(self, x, y):
        return 0 <= x < 15 and 0 <= y < 15

    def get_illegal_actions(self):
        return np.asarray([*map(bool, self.illegal_actions)])

    def check_win(self, ax, ay):
        dirs = [(-1, -1), (0, -1), (1, -1), (1, 0)]
        poss_win = self.state[ay*15+ax]
        for dx, dy in dirs:
            for m in range(-4, 1):
                x = ax+m*dx
                y = ay+m*dy
                if not self.is_in_bounds(x, y) or not self.is_in_bounds(x+4*dx, y+4*dy):
                    continue
                if all(self.state[(y+i*dy)*15+x+i*dx] == poss_win for i in range(5)):
                    return poss_win
        if all(x is not None for x in self.state):
            return 0.5




