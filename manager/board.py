import tkinter as tk
from PIL import Image, ImageTk

BITMAP_CELL_SIZE = 32
CANVAS_CELL_SIZE = 32

class Board:
    def __init__(self, root, size):
        self.root = root
        self.size = size
        self.stones = [[None for _ in range(self.size)] for _ in range(self.size)]
        self.board = [[0 for _ in range(self.size)] for _ in range(self.size)]
        self.history = []
        self.on_turn = 1
        self.winner = None
        self.winning_line = None
        self.moves_enabled = True
        self.undo_enabled = True

        self.callback_move_made = None
        self.callback_move_undo = None

        self.canvas = tk.Canvas(self.root, width=self.size*BITMAP_CELL_SIZE, height=self.size*BITMAP_CELL_SIZE)
        self.canvas.pack(expand=tk.YES, fill=tk.BOTH)
        self.canvas.bind('<Button-1>', self.mouse_left_click)
        self.canvas.bind('<Button-3>', self.mouse_right_click)

        self.bitmap = Image.open('bitmap.png').convert('RGBA')
        self.bmp_board = [[ImageTk.PhotoImage(self.bitmap.crop([x*BITMAP_CELL_SIZE, y*BITMAP_CELL_SIZE, (x+1)*BITMAP_CELL_SIZE, (y+1)*BITMAP_CELL_SIZE])) for x in range(3)] for y in range(3)]
        self.bmp_stones = [ImageTk.PhotoImage(self.bitmap.crop([3*BITMAP_CELL_SIZE, s*BITMAP_CELL_SIZE, 4*BITMAP_CELL_SIZE, (s+1)*BITMAP_CELL_SIZE])) for s in range(2)]

        self.draw_grid()

    def reset(self):
        for row in self.stones:
            for stone in row:
                if stone is not None:
                    self.canvas.delete(stone)
        if self.winning_line is not None:
            self.canvas.delete(self.winning_line)
        self.stones = [[None for _ in range(self.size)] for _ in range(self.size)]
        self.board = [[0 for _ in range(self.size)] for _ in range(self.size)]
        self.history = []
        self.on_turn = 1
        self.winner = None
        self.winning_line = None
        
    def enable(self, moves=None, undo=None):
        if moves is not None:
            self.moves_enabled = moves
        if undo is not None:
            self.undo_enabled = undo

    def is_in_bounds(self, x, y):
        return 0 <= x < self.size and 0 <= y < self.size

    def mouse_left_click(self, event):
        if self.moves_enabled:
            cell_x = int(self.canvas.canvasx(event.x) // CANVAS_CELL_SIZE)
            cell_y = int(self.canvas.canvasy(event.y) // CANVAS_CELL_SIZE)

            if self.is_in_bounds(cell_x, cell_y):
                res = self.make_move(cell_x, cell_y)
                if res and self.callback_move_made is not None:
                    self.callback_move_made()

    def mouse_right_click(self, event):
        if self.undo_enabled:
            res = self.undo_move()
            if res and self.callback_move_undo is not None:
                self.callback_move_undo()

    def draw_grid(self):
        for y in range(15):
            for x in range(15):
                self.canvas.create_image(x*CANVAS_CELL_SIZE, y*CANVAS_CELL_SIZE, image=self.bmp_board[1+(y>0)-(y<14)][1+(x>0)-(x<14)], anchor=tk.NW)

    def make_move(self, x, y):
        if self.board[y][x] == 0 and self.winner is None:
            self.board[y][x] = self.on_turn
            self.stones[y][x] = self.canvas.create_image(x*CANVAS_CELL_SIZE, y*CANVAS_CELL_SIZE, image=self.bmp_stones[self.on_turn - 1], anchor=tk.NW)
            self.on_turn = 3 - self.on_turn
            self.history.append((x, y))
            self.check_win_around(x, y)
            return True
        return False

    def undo_move(self):
        if self.history:
            x, y = self.history.pop()
            self.board[y][x] = 0
            self.canvas.delete(self.stones[y][x])
            self.on_turn = 3 - self.on_turn
            if self.winner is not None:
                self.winner = None
                if self.winning_line is not None:
                    self.canvas.delete(self.winning_line)
                    self.winning_line = None
            return True
        return False

    def check_win_around(self, ax, ay):
        dirs = [(-1, -1), (0, -1), (1, -1), (1, 0)]
        poss_win = self.board[ay][ax]
        for dx, dy in dirs:
            for m in range(-4, 1):
                x = ax+m*dx
                y = ay+m*dy
                if not self.is_in_bounds(x, y) or not self.is_in_bounds(x+4*dx, y+4*dy):
                    continue
                if all(self.board[y+i*dy][x+i*dx] == poss_win for i in range(5)):
                    if (not self.is_in_bounds(x-dx, y-dy) or self.board[y-dy][x-dx] != poss_win) and (not self.is_in_bounds(x+5*dx, y+5*dy) or self.board[y+5*dy][x+5*dx] != poss_win):
                        self.winner = poss_win
                        line_start_x = x * CANVAS_CELL_SIZE + CANVAS_CELL_SIZE // 2
                        line_start_y = y * CANVAS_CELL_SIZE + CANVAS_CELL_SIZE // 2
                        line_end_x = (x+4*dx) * CANVAS_CELL_SIZE + CANVAS_CELL_SIZE // 2
                        line_end_y = (y+4*dy) * CANVAS_CELL_SIZE + CANVAS_CELL_SIZE // 2
                        self.winning_line = self.canvas.create_line(line_start_x, line_start_y, line_end_x, line_end_y, fill='green', width=5)
                        return
        if len(self.history) == self.size ** 2:
            self.winner = 0




