import torch as T
import torch.nn as nn
import torch.nn.functional as F


class DeepQNetwork(nn.Module):
    def __init__(self, input_dims, fc1_dims, fc2_dims, n_actions):
        super(DeepQNetwork, self).__init__()
        self.input_dims = input_dims
        self.fc1_dims = fc1_dims
        self.fc2_dims = fc2_dims
        self.n_actions = n_actions
        self.fc1 = nn.Linear(*self.input_dims, self.fc1_dims)
        self.fc2 = nn.Linear(self.fc1_dims, self.fc2_dims)
        self.fc3 = nn.Linear(self.fc2_dims, self.n_actions)

        self.device = T.device('cuda:0' if T.cuda.is_available() else 'cpu')
        self.to(self.device)

    def forward(self, state):
        x = F.relu(self.fc1(state))
        x = F.relu(self.fc2(x))
        actions = self.fc3(x)

        return actions


class Agent:
    def __init__(self, input_dims, n_actions):
        self.Q_eval = DeepQNetwork(n_actions=n_actions, input_dims=input_dims, fc1_dims=225, fc2_dims=225)

    def choose_action(self, observation, illegal):
        state = T.tensor(observation).unsqueeze(0).to(self.Q_eval.device)
        actions = self.Q_eval.forward(state)
        actions[0][illegal] = -1e20
        action = T.argmax(actions).item()
        return action

    def load(self, filename):
        self.Q_eval.load_state_dict(T.load(filename))