import tkinter as tk

from game import Game

window = tk.Tk()

window.title("Gomoku")

game = Game(window, 15)

window.mainloop()