import tkinter as tk
from tkinter import messagebox, ttk, filedialog
from parse import parse
import os

from board import Board
from brain import Brain, BrainError

class Player:
    def __init__(self, is_human, name):
        self.is_human = is_human
        self.name = name

class HumanPlayer(Player):
    def __init__(self, name):
        super().__init__(True, name)

class EnginePlayer(Player):
    def __init__(self, engine_path):
        super().__init__(False, os.path.basename(engine_path).lstrip('pbrain-').rstrip('.exe'))
        self.engine_path = engine_path
        self.engine = Brain(self.engine_path)
        self.first_move = True

class Game:
    def __init__(self, root, board_size):
        self.root = root
        self.board = Board(self.root, board_size)
        self.board.callback_move_made = self.move_made_on_board
        self.board.callback_move_undo = self.move_undo_on_board
        self.board.enable(undo=False)

        self.players = [HumanPlayer('Human'), HumanPlayer('Human')]
        self.on_turn = 1
        self.error = False

        self.navigation_frame = tk.Frame(self.root, width=self.board.canvas.winfo_width())
        self.navigation_frame.pack(fill=tk.BOTH)
        self.navigation_frame.columnconfigure(1, weight=1)
        self.central_frame = tk.Frame(self.navigation_frame)
        self.central_frame.grid(row=0, column=1)

        left_player_frame = tk.Frame(self.navigation_frame)
        right_player_frame = tk.Frame(self.navigation_frame)
        left_player_frame.grid(row=0, column=0)
        right_player_frame.grid(row=0, column=2)
        self.player_info_frames = [left_player_frame, right_player_frame]

        left_player_name_label = ttk.Label(left_player_frame, text=self.players[0].name)
        right_player_name_label = ttk.Label(right_player_frame, text=self.players[1].name)
        left_player_name_label.pack()
        right_player_name_label.pack()
        self.player_name_labels = [left_player_name_label, right_player_name_label]

        left_player_switch_button = ttk.Button(left_player_frame, text='Engine', command=lambda: self.user_switch_engine(0))
        right_player_switch_button = ttk.Button(right_player_frame, text='Engine', command=lambda: self.user_switch_engine(1))
        left_player_switch_button.pack()
        right_player_switch_button.pack()
        self.player_switch_buttons = [left_player_switch_button, right_player_switch_button]

        self.status_label = ttk.Label(self.navigation_frame, text='New game')
        self.status_label.grid(row=0, column=1)
        self.new_game_button = ttk.Button(self.navigation_frame, text='New game', command=self.new_game)
        self.new_game_button.grid(row=1, column=1)

        self.next_move()

    def switch_player(self, idx, is_human, name=None, engine_path=None):
        if not self.players[idx].is_human:
            self.players[idx].engine.terminate()
        if is_human:
            player = HumanPlayer(name)
            self.players[idx] = player
            self.player_name_labels[idx].config(text=player.name)
            self.player_switch_buttons[idx].config(text='Engine')
        else:
            try:
                player = EnginePlayer(engine_path)
                player.engine.start(self.board.size)
                player.engine.info('timeout_turn', 1000)
                player.engine.info('timeout_match', 10000000)
                player.engine.info('rule', 1)
                player.engine.info('time_left', 10000000)
            except BrainError as e:
                self.error = True
                messagebox.showerror('Engine Error', f'Couldn\'t switch the engine: {e}')
            else:
                self.players[idx] = player
                self.player_name_labels[idx].config(text=player.name)
                self.player_switch_buttons[idx].config(text='Human')

    def user_switch_engine(self, idx):
        print(idx)
        if self.players[idx].is_human:
            filename = filedialog.askopenfilename(filetypes=['GomocupBrain .exe'])
            self.switch_player(idx, False, engine_path=filename)
            if idx == self.on_turn:
                self.update_move()
        else:
            self.switch_player(idx, True, 'Human')

    def new_game(self):
        self.board.reset()
        self.on_turn = 1
        self.error = False
        for player in self.players:
            player.first_move = True
            if not player.is_human:
                try:
                    player.engine.restart()
                except BrainError as e:
                    self.error = True
                    messagebox.showerror('Engine Error', f'{player.name}: Couldn\'t restart the engine: {e}')
        self.next_move()

    def update_move(self):
        if self.error:
            self.status_label.config(text=f'Error. Please start a new game.')
            self.board.enable(moves=False)
            self.new_game_button.config(state=tk.NORMAL)
            self.player_switch_buttons[self.on_turn].config(state=tk.NORMAL)
            self.player_switch_buttons[1 - self.on_turn].config(state=tk.NORMAL)
        else:
            player = self.players[self.on_turn]
            move_nr = len(self.board.history) + 1
            self.status_label.config(text=f'Move #{move_nr}')
            if player.is_human:
                self.board.enable(moves=True)
                self.new_game_button.config(state=tk.NORMAL)
                self.player_switch_buttons[self.on_turn].config(state=tk.NORMAL)
                self.player_switch_buttons[1 - self.on_turn].config(state=tk.NORMAL)
            else:
                self.board.enable(moves=False)
                self.new_game_button.config(state=tk.DISABLED)
                self.player_switch_buttons[self.on_turn].config(state=tk.DISABLED)
                self.player_switch_buttons[1 - self.on_turn].config(state=tk.NORMAL)
                player.engine.info('time_left', 10000000)
                if player.first_move:
                    player.engine.send('BOARD')
                    for i, (x, y) in enumerate(self.board.history):
                        who = [1, 2][i % 2 == self.on_turn % 2]
                        player.engine.send(f'{x},{y},{who}')
                    player.engine.send('DONE')
                    player.first_move = False
                else:
                    last_x, last_y = self.board.history[-1]
                    player.engine.turn(last_x, last_y)
                self.root.after(100, self.check_engine)

    def next_move(self):
        if self.board.winner is None:
            self.on_turn = 1 - self.on_turn
            self.update_move()
        else:
            self.new_game_button.config(state=tk.NORMAL)
            self.player_switch_buttons[self.on_turn].config(state=tk.NORMAL)
            self.player_switch_buttons[1 - self.on_turn].config(state=tk.NORMAL)
            if self.board.winner == 0:
                self.status_label.config(text='It\'s a draw.')
            else:
                win_player = self.players[self.board.winner - 1]
                self.status_label.config(text=f'The winner is {win_player.name}.')

    def check_engine(self):
        engine_x, engine_y = None, None
        player = self.players[self.on_turn]
        for message in player.engine.messages_iter():
            message = message.strip()
            p = parse('{x:d},{y:d}', message)
            if p:
                engine_x, engine_y = p['x'], p['y']
        if engine_x is not None and engine_y is not None:
            res = self.board.make_move(engine_x, engine_y)
            if not res:
                self.error = True
                messagebox.showerror('Engine Error', f'{player.name}: Invalid move [{engine_x}, {engine_y}]')
            self.next_move()
        else:
            self.root.after(100, self.check_engine)

    def move_made_on_board(self):
        self.next_move()

    def move_undo_on_board(self):
        pass

