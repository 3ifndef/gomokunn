from agent import Agent
from enviroment import Game
import numpy as np
from collections import Counter

def main():
    env = Game()
    agent = Agent(0.99, 1, 0.001, [675], 64, 225, 100000, 0.02, 0.0005)

    game_results = []
    dummy_state = np.zeros(675)
    while True:
        env.reset()
        states = []
        actions = []
        rewards = [0, 0]
        on_turn = 0
        while True:
            states.append(env.get_nn_input(on_turn))
            if env.winner is not None:
                rewards[0] = 1 if env.winner == 0 else -1 if env.winner == 1 else 0.5
                rewards[1] = 1 if env.winner == 1 else -1 if env.winner == 0 else 0.5
                game_results.append((1, env.winner))
                break
            actions.append(agent.choose_action(states[-1], env.get_illegal_actions()))
            if not env.is_legal_action(actions[-1]):
                states.append(env.get_nn_input(1 - on_turn))
                rewards[on_turn] = -1
                game_results.append((0, 1 - on_turn))
                break
            env.step(on_turn, actions[-1])
            on_turn = 1 - on_turn
        states.append(dummy_state)
        for player in range(2):
            for i in range(player, len(states) - 2, 2):
                terminal = i + 4 >= len(states)
                agent.store_transition(states[i], actions[i], rewards[player] * terminal, states[i + 2], terminal)
                agent.learn()
        if len(game_results) % 10000 == 0:
            print(f'{len(game_results)} game played')
            counter = Counter(game_results[-10000:])
            for e, v in sorted(counter.items()):
                print(e, v)
            agent.save(f'model{len(game_results)}.pth')


if __name__ == '__main__':
    main()