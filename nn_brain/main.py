from agent import Agent
from parse import parse
import numpy as np
import sys
import pathlib

class Board:
    def __init__(self, size):
        self.size = size
        self.n_squares = self.size * self.size
        self.reset()

    def reset(self):
        self.board = [0 for _ in range(self.n_squares)]
        self.illegal_actions = [1 for i in range(self.n_squares)]
        self.illegal_actions[self.size // 2 * (self.size + 1)] = 0

    def get_nn_input(self):
        return np.asarray([x == 0 for x in self.board] + [x == 1 for x in self.board] + [x == 2 for x in self.board], dtype=np.float32)

    def add_stone(self, x, y, who):
        action = y * self.size + x
        self.board[action] = who
        self.illegal_actions[action] = 2
        for a in range(max(0, x - 2), min(self.size, x + 3)):
            for b in range(max(0, y - 2), min(self.size, y + 3)):
                if self.illegal_actions[b * self.size + a] == 1:
                    self.illegal_actions[b * self.size + a] = 0

    def get_forced_move(self, dpt=1):
        for p in [1, 2]:
            m = []
            for a in range(self.n_squares):
                if self.illegal_actions[a] or self.board[a]:
                    continue
                self.board[a] = p
                w = self.check_win(*self.split_coords(a))
                self.board[a] = 0
                if w:
                    m.append(a)
            if m:
                return m
        if dpt:
            for p in [1, 2]:
                m = []
                b = 1 
                for a in range(self.n_squares):
                    if self.illegal_actions[a] or self.board[a]:
                        continue
                    self.board[a] = p
                    f = len(self.get_forced_move(dpt - 1))
                    self.board[a] = 0
                    if f > b:
                        m.clear()
                        b = f
                    if f >=b:
                        m.append(a)
                if m:
                    return m
        return []

    def check_win(self, ax, ay):
        dirs = [(-1, -1), (0, -1), (1, -1), (1, 0)]
        poss_win = self.board[ay * self.size + ax]
        for dx, dy in dirs:
            for m in range(-4, 1):
                x = ax+m*dx
                y = ay+m*dy
                if x<0 or x>=self.size or y<0 or y>=self.size or x+4*dx<0 or x+4*dx>=self.size or y+4*dy<0 or y+4*dy>=self.size:
                    continue
                if all(self.board[(y+i*dy)*self.size+x+i*dx] == poss_win for i in range(5)):
                    return poss_win
        return 0

    def split_coords(self, action):
        return action % 15, action // 15


def my_move(agent, board):
    forced = board.get_forced_move()
    if len(forced) >= 2:
        print(f'DEBUG more forced moves {forced}')
        action = agent.choose_action(board.get_nn_input(), [a not in forced for a in range(board.n_squares)])
    elif forced:
        print('DEBUG only move')
        action = forced[0]
    else:
        action = agent.choose_action(board.get_nn_input(), [*map(bool, board.illegal_actions)])
    x, y = board.split_coords(action)
    board.add_stone(x, y, 1)
    print(f'{x},{y}')
    return x, y

def main():
    try:
        pth = pathlib.Path(sys.argv[0])
        agent = Agent([675], 225)
        print('DEBUG loading from', str(pth.parent.absolute()) + '\\model.pth')
        agent.load(str(pth.parent.absolute()) + '\\model.pth')
        print('DEBUG model loaded')
    except:
        print('ERROR failed to load the NN')
        exit(0)
    board = Board(15)        
    while True:
        line = input().upper()
        p = parse('START {brd_sz:d}', line)
        if p:
            print('OK' if p['brd_sz'] else 'ERROR unsupported board size')
            board.reset()
            continue
        p = parse('RESTART', line)
        if p:
            board.reset()
            print('OK')
            continue
        p = parse('BEGIN', line)
        if p:
            my_move(agent, board)
            continue
        p = parse('TURN {x:d},{y:d}', line)
        if p:
            board.add_stone(p['x'], p['y'], 2)
            my_move(agent, board)
            continue
        p = parse('BOARD', line)
        if p:
            board.reset()
            while True:
                line = input().upper()
                if line == 'DONE':
                    break
                p = parse('{x:d},{y:d},{w:d}', line)
                if not p:
                    print('ERROR invalid board command')
                    break
                board.add_stone(p['x'], p['y'], p['w'])
            my_move(agent, board)
            continue
        p = parse('END', line)
        if p:
            exit(0)
        p = parse('INFO {} {}', line)
        if p:
            continue
        print('UNKNOWN')

if __name__ == '__main__':
    main()